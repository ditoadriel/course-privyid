package main

import (
	"fmt"
	"sync"
	"time"
)

var conferenceName = "Go conference"

const conferenceTickets int = 50

var remainingTickets uint = 50
var bookings = make([]UserData, 0)

type UserData struct {
	firstName       string
	lastName        string
	email           string
	numberOfTickets uint
}

var wg = sync.WaitGroup{}

func main() {

	greetUsers()

	//user input func
	firstName, lastName, email, userTicket := getUserInput()

	//validate yser func
	isValidName, isValidEmail, isValidTicketNumber := validateUserInput(firstName, lastName, email, userTicket)

	if isValidName && isValidEmail && isValidTicketNumber {
		//bookticket func
		bookTicket(userTicket, firstName, lastName, email)
		//sendticket
		wg.Add(1)
		go sendTicket(userTicket, firstName, lastName, email)
		//Firtstname func
		firstNames := getFirstName()
		fmt.Printf("The first names of booking are: %v\n", firstNames)

		if remainingTickets == 0 {
			fmt.Println("Our conferece is out of tickets.")
		}
	} else {
		notValidInput(isValidName, isValidEmail, isValidTicketNumber)
	}
	wg.Wait()

}

func greetUsers() {
	fmt.Printf("Welcome to %v booking application\n", conferenceName)
	fmt.Printf("We have total %v Ticket and %v still available\n", conferenceTickets, remainingTickets)
	fmt.Println("Get your tiket!")
}

func getFirstName() []string {
	firstNames := []string{}

	for _, booking := range bookings {
		firstNames = append(firstNames, booking.firstName)
	}

	return firstNames
}

func getUserInput() (string, string, string, uint) {
	var firstName string
	var lastName string
	var email string
	var userTicket uint

	fmt.Println("Enter Your firstname: ")
	fmt.Scan(&firstName)

	fmt.Println("Enter Your lastname: ")
	fmt.Scan(&lastName)

	fmt.Println("Enter Your email: ")
	fmt.Scan(&email)

	fmt.Println("Enter Your userTicket: ")
	fmt.Scan(&userTicket)

	return firstName, lastName, email, userTicket
}

func bookTicket(userTicket uint, firstName string, lastName string, email string) {
	remainingTickets = remainingTickets - userTicket

	//struct for user
	var userData = UserData{
		firstName:       firstName,
		lastName:        lastName,
		email:           email,
		numberOfTickets: userTicket,
	}

	bookings = append(bookings, userData)
	fmt.Printf("List of booking is %v\n", bookings)

	fmt.Printf("Thank You %v %v for booking %v tickets, you will reciew a confirmation email at %v\n", firstName, lastName, userTicket, email)

	fmt.Printf("%v tickets remain for %v\n", remainingTickets, conferenceName)
}

func sendTicket(userTickets uint, firstName string, lastName string, email string) {
	time.Sleep(50 * time.Second)
	var ticket = fmt.Sprintf("%v tickets for %v %v", userTickets, firstName, lastName)
	fmt.Println("###################")
	fmt.Printf("sending ticket:\n %v \nto email %v\n", ticket, email)
	fmt.Println("###################")
	wg.Done()
}

func notValidInput(isValidName bool, isValidEmail bool, isValidTicketNumber bool) (bool, bool, bool) {
	if !isValidName {
		fmt.Println("First name or lasr is to short")
	}
	if !isValidEmail {
		fmt.Println("email address you enter doesnt contains @ sign")
	}
	if !isValidTicketNumber {
		fmt.Println("Number of ticket is no valid")
	}

	return isValidName, isValidEmail, isValidTicketNumber
}
