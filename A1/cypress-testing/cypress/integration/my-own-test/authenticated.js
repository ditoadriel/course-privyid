/// <reference types="cypress" />

describe('Basic Auntheticated Desktop Test', () => {

    const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InVqaWNvYmF0ZXN0aW5nIiwiX2lkIjoiNjMzMmEzOGJiZjNhM2UwMDA5N2Y4ZjE5IiwibmFtZSI6InVqaWNvYmEiLCJpYXQiOjE2NjQyNjM1NjEsImV4cCI6MTY2OTQ0NzU2MX0.INJivbWtbN3sqHgFulroR4UUqZuSS3-3dYTC9dBfR8w'

    before(() => {
        cy.then(() => {
            window.localStorage.setItem('__auth__token',token)
        })

    })

    beforeEach(() =>{
        cy.viewport(1280, 720)
        cy.visit('https://codedamn.com')
    })

    it('Should load playground correctly', () => {
        cy.visit('https://codedamn.com/playground/k93vIEJDCmMYfMSBAqNNm')

        cy.contains('Starting codedamn playground', {timeout: 1 * 1000}).should('exist')
        cy.get('div')
        
    })

    it('New file feature works', () => {
        cy.visit('https://codedamn.com/playground/k93vIEJDCmMYfMSBAqNNm')

        const fileName = Math.random()

        cy.get(['data-testid=xtermoverlay'],{timeout: 10 * 1000})
        .type('{ctrl}{c}')
        .type(`touch ${fileName}.js{enter}`)

        cy.contains(`${fileName}.js`).should('exist')
        
    })

    it.only('Rename file feature works', () => {
        cy.visit('https://codedamn.com/playground/k93vIEJDCmMYfMSBAqNNm')

        const fileName = Math.random()

        cy.get(['data-testid=xtermoverlay'],{timeout: 10 * 1000})
        .type('{ctrl}{c}')
        .type(`touch ${fileName}{enter}`)

        cy.contains(`${fileName}.js`).rightclick()

        cy.contains('Rename File').click()

        cy.get('[data-testid=renamefilefolder]').type(`new_.${fileName}.js`)

        cy.get('[data-testid=renamebtn]').click()



        
    })

})