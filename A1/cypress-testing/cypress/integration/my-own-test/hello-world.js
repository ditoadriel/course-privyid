/// <reference types="cypress" />

describe('Basic Unauntheticated Desktop Test', () => {

    beforeEach(() =>{
        cy.viewport(1280, 720)
        cy.visit('https://codedamn.com')
    })

    it('The web page loads, at least', ()=>{

    })

    it('login page looks good', () =>{ 
        cy.contains('Sign in').click()

        cy.contains('Forgot your password?').should('exist')

        cy.contains('Create one').should('exist')
    })

    it('the login page links work', () =>{

        cy.contains('Sign in').click()

        cy.log('Going to forgot password')

        cy.contains('Forgot your password?').click()

        cy.url().should('include','/password-reset')

        cy.url().then(value => {
            cy.log('The current real URL is:', value)
        })

        cy.go('back')

        cy.contains('Create one').click()

        cy.url().should('include','/register')

    })

    it('Login Should display correct error',() =>{

        cy.contains('Sign in').click()
        cy.url().should('include','/login')
        cy.contains('Unable to authorize').should('not.exist')

        cy.get('[data-testid=username]').type('admin')
        cy.get('[data-testid=password]').type('admin')
        cy.get('[data-testid=login]').click()

        cy.contains('Unable to authorize').should('exist')

    })

    it('Login Should work fine',() =>{

        cy.contains('Sign in').click()
        cy.url().should('include','/login')

        cy.get('[data-testid=username]').type('ujicobatesting')
        cy.get('[data-testid=password]').type('DITOadriel12')
        cy.get('[data-testid=login]').click()

        cy.url().should('include','/dashboard')

    })

})